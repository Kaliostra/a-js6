const btn = document.querySelector(".btn");
const url = "https://api.ipify.org/?format=json";
const ulList = document.querySelector(".info");

async function showIP() {
  const response = await fetch(url);
  const data = await response.json();
  console.log(data.ip);
  const IPfound = await fetch(`http://ip-api.com/json/${data.ip}`);
  const answer = await IPfound.json();
  console.log(answer);
  ulList.innerHTML = `
  <li> Country: ${answer.country}</li>
  <li> City: ${answer.city} </li>
  <li> Timezone: ${answer.timezone} </li>
  <li> Region : ${answer.regionName} ${answer.region} </li>
  <li> Countrycode : ${answer.countryCode} </li>
  `;
}

btn.addEventListener("click", showIP);
